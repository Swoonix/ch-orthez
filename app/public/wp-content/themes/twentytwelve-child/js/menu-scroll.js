var header = document.querySelector(".contour-header-image");
var menu = document.querySelector(".primary-nav");

function scrolled(){
	var windowHeight = document.body.clientHeight,
		currentScroll = document.body.scrollTop || document.documentElement.scrollTop;
	
	menu.className = (currentScroll >= header.clientHeight) ? "primary-nav fixed" : "primary-nav";
}

addEventListener("scroll", scrolled, false);
