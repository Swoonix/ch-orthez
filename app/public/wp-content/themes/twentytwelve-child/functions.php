<?php


  function securite_bdd($string)
  {
    // On regarde si le type de string est un nombre entier (int)
    if(ctype_digit($string))
    {
      $string = intval($string);
    }
    // Pour tous les autres types
    else
    {
      $string = mysql_real_escape_string($string);
      $string = addcslashes($string, '%_');
    }
    
    return $string;
  }


function set_container_class ($args) {
$args['container_class'] = str_replace(' ','-',$args['theme_location']).'-nav'; return $args;
}
add_filter ('wp_nav_menu_args', 'set_container_class');

function the_title_trim($title) {

  $title = esc_attr($title);

  $findthese = array(
    '#Protégé:#',
    '#Privé:#'
  );

  $replacewith = array(
    '', // What to replace "Protected:" with
    '' // What to replace "Private:" with
  );

  $title = preg_replace($findthese, $replacewith, $title);
  return $title;
}
add_filter('the_title', 'the_title_trim');

//***Fil d'arianne
//Récupérer les catégories parentes
function myget_category_parents($id, $link = false,$separator ='/',$nicename = false,$visited = array()) {
  $chain = '';$parent = &get_category($id);
    if (is_wp_error($parent))return $parent;
    if ($nicename)$name = $parent->name;
    else $name = $parent->cat_name;
    if ($parent->parent && ($parent->parent != $parent->term_id ) &&!in_array($parent->parent, $visited)) {
        $visited[] = $parent->parent;$chain .= myget_category_parents($parent->parent, $link, $separator, $nicename, $visited );}
    if ($link) $chain .= '<span typeof="v:Breadcrumb"><a href="' .get_category_link( $parent->term_id ) . '" title="Voir tous les articles de '.$parent->cat_name.'" rel="v:url" property="v:title">'.$name.'</a></span>' . $separator;
    else $chain .= $name.$separator;
    return $chain;}

//Le rendu
function mybread() {
  // variables gloables
  global $wp_query;$ped=get_query_var('paged');$rendu = '<div xmlns:v="http://rdf.data-vocabulary.org/#">';  
  $debutlien = '<span id="breadex">Vous &ecirc;tes ici :</span> <span typeof="v:Breadcrumb"><a title="'. get_bloginfo('name') .'" id="breadh" href="'.home_url().'" rel="v:url" property="v:title">'.get_bloginfo('name') .'</a></span>';
  $debut = '<span id="breadex">Vous &ecirc;tes ici :</span> <span typeof="v:Breadcrumb">Accueil de '. get_bloginfo('name') .'</span>';

  // si l'utilisateur a défini une page comme page d'accueil
  if ( is_front_page() ) {$rendu .= $debut;}

  // dans le cas contraire
  else {

    // on teste si une page a été définie comme devant afficher une liste d'article 
    if( get_option('show_on_front') == 'page') {
      $url = urldecode(substr($_SERVER['REQUEST_URI'], 1));
      $uri = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
      $posts_page_id = get_option( 'page_for_posts');
      $posts_page_url = get_page_uri($posts_page_id);  
      $pos = strpos($uri,$posts_page_url);
      if($pos !== false) {
        $rendu .= $debutlien.' &raquo; <span typeof="v:Breadcrumb">Les articles</span>';
      }
      else {$rendu .= $debutlien;} 
    }

    //Si c'est l'accueil
    elseif ( is_home()) {$rendu .= $debut;}

    //pour tout le reste
    else {$rendu .= $debutlien;}

    // les catégories
    if ( is_category() ) {
      $cat_obj = $wp_query->get_queried_object();$thisCat = $cat_obj->term_id;$thisCat = get_category($thisCat);$parentCat =get_category($thisCat->parent);
      if ($thisCat->parent != 0) $rendu .= " &raquo; ".myget_category_parents($parentCat, true, " &raquo; ", true);
      if ($thisCat->parent == 0) {$rendu .= " &raquo; ";}
      if ( $ped <= 1 ) {$rendu .= single_cat_title("", false);}
      elseif ( $ped > 1 ) {
        $rendu .= '<span typeof="v:Breadcrumb"><a href="' .get_category_link( $thisCat ) . '" title="Voir tous les articles de '.single_cat_title("", false).'" rel="v:url" property="v:title">'.single_cat_title("", false).'</a></span>';}}

    // les auteurs
    elseif ( is_author()){
      global $author;$user_info = get_userdata($author);$rendu .= " &raquo; Articles de l'auteur ".$user_info->display_name."</span>";}  

    // les mots clés
    elseif ( is_tag()){
      $tag=single_tag_title("",FALSE);$rendu .= " &raquo; Articles sur le th&egrave;me <span>".$tag."</span>";}
      elseif ( is_date() ) {
          if ( is_day() ) {
              global $wp_locale;
              $rendu .= '<span typeof="v:Breadcrumb"><a href="'.get_month_link( get_query_var('year'),get_query_var('monthnum') ).'" rel="v:url" property="v:title">'.$wp_locale->get_month(get_query_var('monthnum') ).' '.get_query_var('year').'</a></span> ';
              $rendu .= " &raquo; Archives pour ".get_the_date();}
      else if ( is_month() ) {
              $rendu .= " &raquo; Archives pour ".single_month_title(' ',false);}
      else if ( is_year() ) {
              $rendu .= " &raquo; Archives pour ".get_query_var('year');}}

    //les archives hors catégories
    elseif ( is_archive() && !is_category()){
          $posttype = get_post_type();
      $tata = get_post_type_object( $posttype );
      $var = '';
      $the_tax = get_taxonomy( get_query_var( 'taxonomy' ) );
      $titrearchive = $tata->labels->menu_name;
      if (!empty($the_tax)){$var = $the_tax->labels->name.' ';}
          if (empty($the_tax)){$var = $titrearchive;}
      $rendu .= ' &raquo; Archives sur "'.$var.'"';}

    // La recherche
    elseif ( is_search()) {
      $rendu .= " &raquo; R&eacute;sultats de votre recherche <span>&raquo; ".get_search_query()."</span>";}

    // la page 404
    elseif ( is_404()){
      $rendu .= " &raquo; 404 Page non trouv&eacute;e";}

    //Un article
    elseif ( is_single()){
      $category = get_the_category();
      $category_id = get_cat_ID( $category[0]->cat_name );
      if ($category_id != 0) {
        $rendu .= " &raquo; ".myget_category_parents($category_id,TRUE,' &raquo; ')."<span>".the_title('','',FALSE)."</span>";}
      elseif ($category_id == 0) {
        $post_type = get_post_type();
        $tata = get_post_type_object( $post_type );
        $titrearchive = $tata->labels->menu_name;
        $urlarchive = get_post_type_archive_link( $post_type );
        $rendu .= ' &raquo; <span typeof="v:Breadcrumb"><a href="'.$urlarchive.'" title="'.$titrearchive.'" rel="v:url" property="v:title">'.$titrearchive.'</a></span> &raquo; <span>'.the_title('','',FALSE).'</span>';}}

    //Une page
    elseif ( is_page()) {
      $post = $wp_query->get_queried_object();
      if ( $post->post_parent == 0 ){$rendu .= " &raquo; ".the_title('','',FALSE)."";}
      elseif ( $post->post_parent != 0 ) {
        $title = the_title('','',FALSE);$ancestors =array_reverse(get_post_ancestors($post->ID));array_push($ancestors,$post->ID);
        foreach ( $ancestors as $ancestor ){
          if( $ancestor != end($ancestors) ){$rendu .= '&raquo; <span typeof="v:Breadcrumb"><a href="'. get_permalink($ancestor) .'" rel="v:url" property="v:title">'. strip_tags( apply_filters('single_post_title', get_the_title( $ancestor ) ) ) .'</a></span>';}
          else {$rendu .= ' &raquo; '.strip_tags(apply_filters('single_post_title',get_the_title($ancestor))).'';}}}}
    if ( $ped >= 1 ) {$rendu .= ' (Page '.$ped.')';}
  }
  $rendu .= '</div>';
  echo $rendu;}



  if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'Actu',
        'before_widget' => '<div id="header">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Footer',
        'before_widget' => '<div id="footer">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
  }

add_filter('comments_open', 'wpc_comments_closed', 10, 2);

function wpc_comments_closed( $open, $post_id ) {
$post = get_post( $post_id );
$open = false;
return $open;
}

//Remove 2012 Mobile Javascript
function de_script() {
    wp_dequeue_script( 'twentytwelve-navigation' );
    wp_deregister_script( 'twentytwelve-navigation' );
}

add_action( 'wp_print_scripts', 'de_script', 100 );

function themeprefix_scripts_styles(){
wp_enqueue_style ('slickcss', get_stylesheet_directory_uri() . '/css/slicknav.css','', '1', 'all');
wp_enqueue_script ('slickjs', get_stylesheet_directory_uri() . '/js/ jquery.slicknav.min.js', array( 'jquery' ),'1',true);
wp_enqueue_script( 'scroll',get_template_directory_uri() . '/js/scroll.js', array() );
}
add_action( 'wp_enqueue_scripts', 'themeprefix_scripts_styles');
  
function slicknav_fire() {
$output="<script>
jQuery(function() {
jQuery('.nav-menu').slicknav();
});
</script>";
echo $output;
}
add_action('wp_head','slicknav_fire');




function get_excerpt(){
$excerpt = get_the_content();
$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
$excerpt = strip_shortcodes($excerpt);
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0,200);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
return $excerpt;
}
 
function jc_post_by_category_move($atts, $content = null) {
    extract(shortcode_atts(array(
        "nb" => '5',
        "orderby" => 'post_date',
        "order" => 'DESC',
        "category" => '2'
    ), $atts));
    global $post;
    $tmp_post = $post;
    $out = '';
    $myposts = get_posts('showposts='.$nb.'&orderby='.$orderby.'&category='.$category);

    foreach($myposts as $post){

        setup_postdata( $post );
   	$out .= ''.the_title("","",false).' ('.the_date("","","",false).')';
        $out .= '<marquee>'.get_the_excerpt().'<a href="'.get_permalink().'"> Lire la suite</a>';
    }
    $out .= '</marquee>';
    wp_reset_postdata();
    $post = $tmp_post;
    return $out;
}
add_shortcode("post-by-category-move", "jc_post_by_category_move");

function jc_post_by_category($atts, $content = null) {
    extract(shortcode_atts(array(
        "nb" => '5',
        "orderby" => 'post_date',
        "order" => 'DESC',
        "category" => '2'
    ), $atts));
    global $post;
    $tmp_post = $post;
    $out = '';
    $i = 0;
    $myposts = get_posts('showposts='.$nb.'&orderby='.$orderby.'&category_name='.$category.'&offset=0');
    foreach($myposts as $post){
        setup_postdata( $post );
	$i = $i + 1 ;
	$out .= '';
	$out .= '<div class="actu'.$i.' actu"><div class="actu_image">';
	if ( has_post_thumbnail()) { // dans la boucle
		$out .= ''.get_the_post_thumbnail($post_id, array( 200, 200)).'';	
	}
   	$out .= '</div><div class="actu_texte"><div class="actu_titre"><strong>'.the_title("","",false).'</strong></div>';
        $out .= '<div class="actu_content"><em>'.get_the_excerpt().'</em><a href="'.get_permalink().'"> Lire la suite</a></div></div></div>	';
    }
    $out .= '';
    wp_reset_postdata();
    $post = $tmp_post;
    return $out;
}
add_shortcode("post-by-category", "jc_post_by_category");


function jc_post_by_category2($atts, $content = null) {
    extract(shortcode_atts(array(
        "nb" => '6',
        "orderby" => 'post_date',
        "order" => 'DESC',
        "category" => '1'
    ), $atts));
    global $post;
    $tmp_post = $post;
    $out = '';
    $i = 0;
    $myposts = get_posts('showposts='.$nb.'&orderby='.$orderby.'&category_name='.$category.'&offset=3');
    foreach($myposts as $post){
        setup_postdata( $post );
	$i = $i + 1 ;
	$out .= '';
	$out .= '<div class="actu'.$i.' actu"><div class="actu_image">';
	if ( has_post_thumbnail()) { // dans la boucle
		$out .= ''.get_the_post_thumbnail($post_id, array( 200, 200)).'';	
	}
   	$out .= '</div><div class="actu_texte"><div class="actu_titre"><strong>'.the_title("","",false).'</strong></div>';
        $out .= '<div class="actu_content"><em>'.get_the_excerpt().'</em><a href="'.get_permalink().'"> Lire la suite</a></div></div></div>	';
    }
    $out .= '';
    wp_reset_postdata();
    $post = $tmp_post;
    return $out;
}
add_shortcode("post-by-category2", "jc_post_by_category2");

 

 
 ?>
