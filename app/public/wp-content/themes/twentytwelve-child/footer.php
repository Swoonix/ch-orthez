﻿<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
		</div><!-- #main .wrapper -->
	    </div><!-- on ajoute ici la fermeture de notre conteneur -->
	<footer id="colophon" 	>
		<div class="cadre-site-info">
		<div class="site-info">

			<br /><strong>
			Centre Hospitalier d'Orthez - Rue du moulin - B.P 118 - 64301 ORTHEZ Cedex
			Tel : 05 59 69 70 70 - Fax : 05 59 69 70 00<br /> <br />
			<a href="http://www.ch-orthez.fr/contact/">contact</a> - <a href="http://www.ch-orthez.fr/mentions-legales/">mentions légales</a> - hébergé par <a href="http://www.heliantis.fr">Heliantis</a>
			<br /><br />
			</strong>
		</div><!-- .site-info -->
		</div><!-- .cadre-site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<script type="text/javascript" src="http://www.ch-orthez.fr/wp-content/themes/twentytwelve-child/js/front1.js"></script>
<script type="text/javascript" src="http://www.ch-orthez.fr/wp-content/themes/twentytwelve-child/js/defile.js"></script>
<script type="text/javascript" src="http://www.ch-orthez.fr/wp-content/themes/twentytwelve-child/js/slidcht.js"></script>
<script type="text/javascript" src="http://www.ch-orthez.fr/wp-content/themes/twentytwelve-child/js/menu-scroll.js"></script>
<script type="text/javascript" src="http://www.ch-orthez.fr/wp-content/themes/twentytwelve-child/js/jquery.slicknav.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
