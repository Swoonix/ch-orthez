<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y0DM0p/Vus2PC3rTI3PiE2UgM70ZZl90mobU+93jKZ3f/MSRsGnmt9DtuSzmBdzx1+2S/4Qd09EIfAVl4y1NzQ==');
define('SECURE_AUTH_KEY',  'KDhwTi/l/DmE6ZVYnbKZP3MOTqtDerW+LG4hKEjyYdM9OqMfUpDpGtSkgBMdRZYvCDOUxHrlBYXCR2DDQQdknw==');
define('LOGGED_IN_KEY',    '8wbvaA3Uq6iLyiLZhByvyYu0Nx/juSWfFFSkAUN1U7OaHkv0dTU56ncG1YsUk+w5yy1e9Ka/r6g1aQzpPhrR8Q==');
define('NONCE_KEY',        'LSr48qhJ4EWL+NljXQMpNclN/aDm+8vvj9MCG+09224TLCsw1SkT+X/wqwOYGMoP0ZE/KoU/+l5xzKSEGyTj+A==');
define('AUTH_SALT',        'PhHjbK892mXmpp7BbJMzXtkyZ2idhBp8hJh1sZNUJ/FqszWYrJvazIjgG4aCLmHqzI/YtSMke+HhOLUQA1MblA==');
define('SECURE_AUTH_SALT', '+4NeC/DoqgFbz2SOB+z54rc5rrR4NGsribqTCJG8j25bqSkfPcx4Y5eFkBIsD0QjuxoIDXb38M9pPHYKedzFmA==');
define('LOGGED_IN_SALT',   'V7PGz7v74Jy3SdHzPOo3Ve8lBOUW6adNweoAjsL9P2JOpsiIQIaFu890vFYVrrvet+bmSJPVXvZA5vgUGr74dA==');
define('NONCE_SALT',       'otBeYFcm69tcNEE/luTdclzufJIElTM7DWHflih018kkl2CFjGn26/+FOxAAxHXYGsP9UAQkAblvrwzqxUf+1A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
